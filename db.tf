# Required variables for data resource configuration
variable "dbname" {
  description = "name of database to be provisioned"
  type = string
}
variable "username" {
  description = "root username"
  type = string
}
variable "password" {
  description = "root password"
  type = string
  sensitive = true
}

# Configure data resources 
resource "aws_db_subnet_group" "mjrod_ghost_db_subnet_group" {
  subnet_ids = [aws_subnet.mjrod_ghost_private_subnet_2a.id, aws_subnet.mjrod_ghost_private_subnet_2b.id]
  tags = {
    "Name" = "mjrod_ghost_db_subnet_group"
    "env" = "test"
  }
}

resource "aws_db_instance" "mjrod_ghost_db_instance" {
  allocated_storage = 20
  max_allocated_storage = 100
  db_subnet_group_name = aws_db_subnet_group.mjrod_ghost_db_subnet_group.id
  engine = "mysql"
  engine_version = "8.0.23"
  instance_class = "db.t2.micro"
  name = var.dbname 
  username = var.username 
  password = var.password 
  skip_final_snapshot = true
  vpc_security_group_ids = [aws_security_group.mjrod_ghost_private_subnet_security_group.id]
  
  tags = {
    "Name" = "mjrod_ghost_db_instance"
    "env" = "test"
  }
}