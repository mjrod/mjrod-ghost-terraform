# Configure VPC resource
resource "aws_vpc" "mjrod_ghost_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  tags = {
    "Name" = "mjrod_ghost_vpc",
    "env" = "test"
  }
}

# Configure internet gateway resource
resource "aws_internet_gateway" "mjrod_ghost_igw" {
  vpc_id = aws_vpc.mjrod_ghost_vpc.id
  tags = {
    "Name" = "mjrod_ghost_igw"
    "env" = "test"
  }
}

# Configure route table to allow igw to public subnet communication
resource "aws_route_table" "mjrod_ghost_route_table" {
  vpc_id = aws_vpc.mjrod_ghost_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.mjrod_ghost_igw.id
  }
  tags = {
    "Name" = "mjrod_ghost_route_table"
    "env" = "test"
  }
}
resource "aws_route_table_association" "mjrod_ghost_public_rtb_association" {
  subnet_id = aws_subnet.mjrod_ghost_public_subnet.id
  route_table_id = aws_route_table.mjrod_ghost_route_table.id
}

# Configure public subnet resource
resource "aws_subnet" "mjrod_ghost_public_subnet" {
  vpc_id = aws_vpc.mjrod_ghost_vpc.id
  cidr_block = "10.0.1.0/24"
  tags = {
    "Name" = "mjrod_ghost_public_subnet"
    "env" = "test"
  }
}

# Configure private subnet resorces
resource "aws_subnet" "mjrod_ghost_private_subnet_2a" {
  vpc_id = aws_vpc.mjrod_ghost_vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone_id = "use2-az1"
  tags = {
    "Name" = "mjrod_ghost_private_subnet_2a"
    "env" = "test"
  }
}
resource "aws_subnet" "mjrod_ghost_private_subnet_2b" {
  vpc_id = aws_vpc.mjrod_ghost_vpc.id
  cidr_block = "10.0.3.0/24"
  availability_zone_id = "use2-az2"
  tags = {
    "Name" = "mjrod_ghost_private_subnet_2b"
    "env" = "test"
  }
}