#!/bin/bash
sudo apt update && sudo apt upgrade -y
sudo apt-get install nginx -y
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash
sudo apt-get install nodejs -y
sudo npm install ghost-cli@latest -g
sudo mkdir -p /var/www/mjrod-ghost
sudo chown ubuntu:ubuntu /var/www/mjrod-ghost
sudo chmod 775 /var/www/mjrod-ghost