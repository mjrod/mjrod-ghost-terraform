# Configure SSH Key Pair
resource "tls_private_key" "ssh_key_pair" {
  algorithm = "RSA"
  rsa_bits = 4096
}
resource "aws_key_pair" "mjrod-ghost-key-pair" {
  key_name = "mjrod-ghost-key-pair"
  public_key = tls_private_key.ssh_key_pair.public_key_openssh

  provisioner "local-exec" {
    command = "echo '${tls_private_key.ssh_key_pair.private_key_pem}' > ./mjrod-ghost-key-pair.pem"
  }
}