# Configure compute resources
resource "aws_instance" "mjrod_ghost_ec2_instance" {
  ami = "ami-00399ec92321828f5" #us-east-2
  instance_type = "t2.micro"
  associate_public_ip_address = true
  subnet_id = aws_subnet.mjrod_ghost_public_subnet.id
  vpc_security_group_ids = [aws_security_group.mjrod_ghost_public_subnet_security_group.id]
  key_name = aws_key_pair.mjrod-ghost-key-pair.id
  user_data = "${file("install-ghost.sh")}"

  tags = {
    "Name" = "mjrod_ghost_ec2_instance",
    "env" = "test"
  }
}

output "mjrod_ghost_ec2_instance_ip" {
  value = aws_instance.mjrod_ghost_ec2_instance.public_ip
}